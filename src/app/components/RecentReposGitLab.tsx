import React from 'react'
import { gql, useQuery } from '@apollo/client'
import moment from 'moment'
import Card from './CardGitLab'
import Loading from './Loading'

const GET_RECENT_REPOS = gql`
query GetRecentRepos {
 project(fullPath: "tekhub/lightcv") {
    name
    webUrl
    description
    createdAt
    repository {
      rootRef
      tree {
        lastCommit {
          message
          authoredDate
        }
      }
    }
    projectMembers {
      nodes {
        id
      }
    }
  }
} 
`

interface RecentReposProps {}

export const RecentRepos: React.FC<RecentReposProps> = () => {
  const { loading, error, data } = useQuery(GET_RECENT_REPOS)

  let lastCommitTime
  lastCommitTime =
    data &&
    moment(
      data.project.createdAt
    ).fromNow()
  return (
    <>
      <div className='mt-8'>
        <h2 className='flex flex-col items-baseline py-2 text-xl font-bold md:flex-row'>
          Recent Activity{' '}
          {data && (
            <div className='text-sm font-normal text-gray-30 md:ml-2'>
              (last commit {lastCommitTime}){' '}
              {lastCommitTime.match(/[ms]/) && (
                <>
                  <span role='img' aria-label='sparkles emoji'>
                    {' '}
                    ✨️
                  </span>
                </>
              )}
            </div>
          )}
        </h2>
        <div className='grid grid-cols-1 gap-8 lg:grid-cols-3'>
          <Loading loading={loading} source='GitLab' />
          {error && `Error! ${error.message}`}
          {data &&
            data.project && (
              //.map((r: any) => (
                <Card key={data.project.name} repoData={data.project} useImage={false} />
              //))
              //.reverse()
          )}
        </div>
      </div>
    </>
  )
}

export default RecentRepos
