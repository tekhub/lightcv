export const queryString = `
{
  user(id: "gid://gitlab/User/7129429") {
    id
    name
    avatarUrl
    email
    state
    status {
      emoji
      message
      messageHtml
    }
    userPermissions {
      createSnippet
    }
    username
    webPath
    webUrl
    starredProjects {
      nodes {
        name
      }
    }
    groupMemberships(first: 0) {
      nodes {
        id
      }
    }
    projectMemberships {
      edges {
        node {
          project {
            name
            fullPath
          }
        }
      }
    }
  }
  project(fullPath: "tekhub/lightcv") {
    name
    webUrl
    description
    createdAt
    repository {
      rootRef
      tree {
        lastCommit {
          message
          authoredDate
        }
      }
    }
    projectMembers {
      nodes {
        id
      }
    }
  }
}
`

export default queryString